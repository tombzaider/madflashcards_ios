//
//  EditAlertViewController.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/18/21.
//

import UIKit
import CoreData

class EditAlertViewController: UIViewController {

    @IBOutlet var alertView: UIView!
    @IBOutlet var termEditText: UITextField!
    @IBOutlet var definitionEditText: UITextField!
    //card from FlashCardSetDetailViewController
    var card: Flashcard = Flashcard()
    //use this later to do things to the flashcards potentially
    var parentVC: FlashCardSetDetailViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setup()
    }
    
    func setup()
    {
        alertView.layer.cornerRadius = 8.0
        //set term/def
        termEditText.text = card.term
        definitionEditText.text = card.definition
        //make it so it shows this is editable
        termEditText.becomeFirstResponder()
    }
    
    @IBAction func deleteFlashcard(_ sender: Any) {
        //nothing yet but eventually delete the flashcard
        let deletedCard = parentVC!.cards[parentVC!.selectedIndex]
        
        
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext =
        appDelegate.persistentContainer.viewContext
        managedContext.delete(deletedCard)
        parentVC?.cards.remove(at: parentVC!.selectedIndex)
        parentVC?.tableView.reloadData()
        
        self.dismiss(animated: false, completion: {})
        
        
    }
    
    @IBAction func doneEditing(_ sender: Any) {
        self.dismiss(animated: false, completion: { [self] in
            //do something / save the edits
            
            let deletedCard = parentVC!.cards[parentVC!.selectedIndex]
            
            
            
            guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            let managedContext =
            appDelegate.persistentContainer.viewContext
            managedContext.delete(deletedCard)
            parentVC?.cards.remove(at: parentVC!.selectedIndex)
            
          

            let entity =
            NSEntityDescription.entity(forEntityName: "Flashcard",
                                      in: managedContext)!
            let card = NSManagedObject(entity: entity,
                                      insertInto: managedContext) as! Flashcard
            card.term = termEditText.text
            card.definition = definitionEditText.text
            
            do {
                try managedContext.save()
                parentVC!.cards.append(card)
                parentVC!.tableView.reloadData()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
         

            
            
        })
    }
    
}
